package com.mycompany.agenciadeviagens.dao;

import com.mycompany.agenciadeviagens.dao.conexao.Conexao;
import com.mycompany.agenciadeviagens.model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClienteDAO {
    
    public void inserir(Cliente cliente) throws SQLException{
        Connection con = Conexao.getConexao();

        String SQL_INSERIR = "INSERT INTO gustavo_dutra.cliente"
                + "(cpf, nome, dataNascimento, telefone, email) "
                + "VALUES (?, ?, ?, ?, ?)";
        
        PreparedStatement comando = con.prepareStatement(SQL_INSERIR);

        comando.setString(1, cliente.getCpf());
        comando.setString(2, cliente.getNome());
        comando.setDate(3, cliente.getDataNascimento());
        comando.setString(4, cliente.getTelefone());
        comando.setString(5, cliente.getEmail());

        comando.executeUpdate();
    }
    
    public void atualizar(Cliente cliente) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_ATUALIZAR = "UPDATE gustavo_dutra.cliente "
                + "SET nome = ?, dataNascimento = ?, telefone = ?, email = ? "
                + "WHERE cpf = ?";
        PreparedStatement comando = con.prepareStatement(SQL_ATUALIZAR);

        comando.setString(1, cliente.getNome());
        comando.setDate(2, cliente.getDataNascimento());
        comando.setString(3, cliente.getTelefone());
        comando.setString(4, cliente.getEmail());
        comando.setString(5, cliente.getCpf());
        
        comando.execute();
    }
    
    public void remover(Cliente cliente) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_REMOVER = "DELETE FROM gustavo_dutra.cliente WHERE cpf = ?";
        PreparedStatement comando = con.prepareStatement(SQL_REMOVER);

        comando.setString(1, cliente.getCpf());
        
        comando.execute();
    }
    
    public Cliente consulta(Cliente cliente) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_CONSULTAR = "SELECT * FROM gustavo_dutra.cliente WHERE cpf = ?";
        PreparedStatement ps = con.prepareStatement(SQL_CONSULTAR);
        ps.setString(1, cliente.getCpf());

        ResultSet comando = ps.executeQuery();

        if (comando.next()) {
            Cliente c = new Cliente();

            c.setNome(comando.getString("nome"));
            c.setEmail(comando.getString("email"));
            c.setTelefone(comando.getString("telefone"));
            c.setDataNascimento(comando.getDate("dataNascimento"));
            
            return c;
        }
        return null;
    }
    
    public ArrayList<Cliente> listar() throws SQLException{
        ArrayList<Cliente> clientes = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL_LISTAR = "SELECT * FROM gustavo_dutra.cliente";
        PreparedStatement comando = con.prepareStatement(SQL_LISTAR);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Cliente cliente = new Cliente();

            cliente.setCpf(rs.getString("cpf"));
            cliente.setNome(rs.getString("nome"));
            cliente.setEmail(rs.getString("email"));
            cliente.setTelefone(rs.getString("telefone"));
            cliente.setDataNascimento(rs.getDate("dataNascimento"));

            clientes.add(cliente);
        }
        return clientes;
    }
}
