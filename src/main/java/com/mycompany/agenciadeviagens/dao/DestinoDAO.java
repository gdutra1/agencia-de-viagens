package com.mycompany.agenciadeviagens.dao;

import com.mycompany.agenciadeviagens.dao.conexao.Conexao;
import com.mycompany.agenciadeviagens.model.Destino;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DestinoDAO {
    
    public void inserir(Destino destino) throws SQLException{
        Connection con = Conexao.getConexao();

        String SQL_INSERIR = "INSERT INTO gustavo_dutra.destino"
                + "(idDestino, pais, cidade) "
                + "VALUES (?, ?, ?)";
        
        PreparedStatement comando = con.prepareStatement(SQL_INSERIR);

        comando.setInt(1, destino.getIdDestino());
        comando.setString(2, destino.getPais());
        comando.setString(3, destino.getCidade());

        comando.executeUpdate();
    }
    
    public void atualizar(Destino destino) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_ATUALIZAR = "UPDATE gustavo_dutra.destino "
                + "SET pais = ?, cidade = ? "
                + "WHERE idDestino = ?";
        PreparedStatement comando = con.prepareStatement(SQL_ATUALIZAR);

        comando.setString(1, destino.getPais());
        comando.setString(2, destino.getCidade());
        comando.setInt(3, destino.getIdDestino());
       
        comando.execute();
    }
    
    public void remover(Destino destino) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_REMOVER = "DELETE FROM gustavo_dutra.destino WHERE idDestino = ?";
        PreparedStatement comando = con.prepareStatement(SQL_REMOVER);

        comando.setInt(1, destino.getIdDestino());
        
        comando.execute();
    }
    
    public Destino consulta(Destino destino) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_CONSULTAR = "SELECT * FROM gustavo_dutra.destino WHERE idDestino = ?";
        PreparedStatement ps = con.prepareStatement(SQL_CONSULTAR);
        ps.setInt(1, destino.getIdDestino());

        ResultSet comando = ps.executeQuery();

        if (comando.next()) {
            Destino d = new Destino();

            d.setIdDestino(comando.getInt("idDestino"));
            d.setCidade(comando.getString("cidade"));
            d.setPais(comando.getString("pais"));
            
            return d;
        }
        return null;
    }
    
    public ArrayList<Destino> listar() throws SQLException{
        ArrayList<Destino> destinos = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL_LISTAR = "SELECT * FROM gustavo_dutra.destino";
        PreparedStatement comando = con.prepareStatement(SQL_LISTAR);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Destino destino = new Destino();

            destino.setIdDestino(rs.getInt("idDestino"));
            destino.setCidade(rs.getString("cidade"));
            destino.setPais(rs.getString("pais"));

            destinos.add(destino);
        }
        return destinos;
    }
}
