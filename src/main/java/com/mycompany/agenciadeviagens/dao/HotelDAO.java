package com.mycompany.agenciadeviagens.dao;

import com.mycompany.agenciadeviagens.dao.conexao.Conexao;
import com.mycompany.agenciadeviagens.model.Hotel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HotelDAO {

    public void inserir(Hotel hotel) throws SQLException{
        Connection con = Conexao.getConexao();

        String SQL_INSERIR = "INSERT INTO gustavo_dutra.hotel"
                + "(idHotel, nome, endereco, estrelas, valorDiaria) "
                + "VALUES (?, ?, ?, ?, ?)";
        
        PreparedStatement comando = con.prepareStatement(SQL_INSERIR);

        comando.setInt(1, hotel.getIdHotel());
        comando.setString(2, hotel.getNome());
        comando.setString(3, hotel.getEndereco());
        comando.setInt(4, hotel.getEstrelas());
        comando.setFloat(5, hotel.getValorDiaria());

        comando.executeUpdate();
    }
    
    public void atualizar(Hotel hotel) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_ATUALIZAR = "UPDATE gustavo_dutra.hotel "
                + "SET nome = ?, endereco = ?, estrelas = ?, valorDiaria = ? "
                + "WHERE idHotel = ?";
        PreparedStatement comando = con.prepareStatement(SQL_ATUALIZAR);

        comando.setString(1, hotel.getNome());
        comando.setString(2, hotel.getEndereco());
        comando.setInt(3, hotel.getEstrelas());
        comando.setFloat(4, hotel.getValorDiaria());
        comando.setInt(5, hotel.getIdHotel());
       
        comando.execute();
    }
    
    public void remover(Hotel hotel) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_REMOVER = "DELETE FROM gustavo_dutra.hotel WHERE idHotel = ?";
        PreparedStatement comando = con.prepareStatement(SQL_REMOVER);

        comando.setInt(1, hotel.getIdHotel());
        
        comando.execute();
    }
    
    public Hotel consulta(Hotel hotel) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_CONSULTAR = "SELECT * FROM gustavo_dutra.hotel WHERE idHotel = ?";
        PreparedStatement ps = con.prepareStatement(SQL_CONSULTAR);
        ps.setInt(1, hotel.getIdHotel());

        ResultSet comando = ps.executeQuery();

        if (comando.next()) {
            Hotel h = new Hotel();

            h.setIdHotel(comando.getInt("idHotel"));
            h.setNome(comando.getString("nome"));
            h.setEndereco(comando.getString("endereco"));
            h.setEstrelas(comando.getInt("estrelas"));
            h.setValorDiaria(comando.getFloat("valorDiaria"));
            
            return h;
        }
        return null;
    }
    
    public ArrayList<Hotel> listar() throws SQLException{
        ArrayList<Hotel> hoteis = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL_LISTAR = "SELECT * FROM gustavo_dutra.hotel";
        PreparedStatement comando = con.prepareStatement(SQL_LISTAR);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Hotel hotel = new Hotel();

            hotel.setIdHotel(rs.getInt("idHotel"));
            hotel.setNome(rs.getString("nome"));
            hotel.setEndereco(rs.getString("endereco"));
            hotel.setEstrelas(rs.getInt("estrelas"));
            hotel.setValorDiaria(rs.getFloat("valorDiaria"));

            hoteis.add(hotel);
        }
        return hoteis;
    }
}
