package com.mycompany.agenciadeviagens.dao;

import com.mycompany.agenciadeviagens.dao.conexao.Conexao;
import com.mycompany.agenciadeviagens.model.Destino;
import com.mycompany.agenciadeviagens.model.Voo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VooDAO {
    
    public void inserir(Voo voo) throws SQLException{
        Connection con = Conexao.getConexao();

        String SQL_INSERIR = "INSERT INTO gustavo_dutra.voo"
                + "(idVoo, preco, diaSaida, localSaida, destinacao) "
                + "VALUES (?, ?, ?, ?, ?)";
        
        PreparedStatement comando = con.prepareStatement(SQL_INSERIR);

        comando.setInt(1, voo.getIdVoo());
        comando.setFloat(2, voo.getPreco());
        comando.setDate(3, voo.getDiaSaida());
        comando.setString(4, voo.getLocalSaida());
        comando.setInt(5, voo.getDestinacao().getIdDestino());

        comando.executeUpdate();
    }
    
    public void atualizar(Voo voo) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_ATUALIZAR = "UPDATE gustavo_dutra.voo "
                + "SET preco = ?, diaSaida = ?, localSaida = ?, destinacao = ? "
                + "WHERE idVoo = ?";
        PreparedStatement comando = con.prepareStatement(SQL_ATUALIZAR);

        comando.setFloat(1, voo.getPreco());
        comando.setDate(2, voo.getDiaSaida());
        comando.setString(3, voo.getLocalSaida());
        comando.setInt(4, voo.getDestinacao().getIdDestino());
        comando.setInt(5, voo.getIdVoo());
        
        comando.execute();
    }
    
    public void remover(Voo voo) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_REMOVER = "DELETE FROM gustavo_dutra.voo WHERE idVoo = ?";
        PreparedStatement comando = con.prepareStatement(SQL_REMOVER);

        comando.setInt(1, voo.getIdVoo());
        
        comando.execute();
    }
    
    public Voo consulta(Voo voo) throws SQLException{
        Connection con = Conexao.getConexao();
        String SQL_CONSULTAR = "SELECT * FROM gustavo_dutra.voo WHERE idVoo = ?";
        PreparedStatement ps = con.prepareStatement(SQL_CONSULTAR);
        ps.setInt(1, voo.getIdVoo());

        ResultSet comando = ps.executeQuery();

        if (comando.next()) {
            Voo v = new Voo();

            v.setIdVoo(comando.getInt("idVoo"));
            v.setPreco(comando.getFloat("preco"));
            v.setDiaSaida(comando.getDate("diaSaida"));
            v.setLocalSaida(comando.getString("localSaida"));
            v.setDestinacao(new DestinoDAO().consulta(new Destino(comando.getInt("destinacao"))));
            
            return v;
        }
        return null;
    }
    
    public ArrayList<Voo> listar() throws SQLException{
        ArrayList<Voo> voos = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL_LISTAR = "SELECT * FROM gustavo_dutra.voo";
        PreparedStatement comando = con.prepareStatement(SQL_LISTAR);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Voo v = new Voo();

            v.setIdVoo(rs.getInt("idVoo"));
            v.setPreco(rs.getFloat("preco"));
            v.setDiaSaida(rs.getDate("diaSaida"));
            v.setLocalSaida(rs.getString("localSaida"));
            v.setDestinacao(new DestinoDAO().consulta(new Destino(rs.getInt("destinacao"))));

            voos.add(v);
        }
        return voos;
    }
}
