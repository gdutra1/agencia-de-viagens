package com.mycompany.agenciadeviagens.dao.conexao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexao {
    
    private static String url = "jdbc:postgresql://200.18.128.65/aula";
    private static String usuario = "aula";
    private static String senha = "aula";

    private static Connection conexao = null;

    public static Connection getConexao() {
        try {
            Conexao.conexao = DriverManager.getConnection(url, usuario, senha);
        } catch (Exception e) {
            Conexao.conexao = null;
        }
        return Conexao.conexao;
    }
    
}
