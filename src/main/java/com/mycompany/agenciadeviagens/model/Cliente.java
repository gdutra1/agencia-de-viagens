package com.mycompany.agenciadeviagens.model;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Cliente {
    
    private String cpf;
    private String nome;
    private Date dataNascimento;
    private String telefone;
    private String email;
    
    public Cliente(){}
    
    public Cliente(String cpf){
        this.cpf = cpf;
    }

    public Cliente(String cpf, String nome, Date dataNascimento, String telefone, String email) {
        this.cpf = cpf;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
        this.email = email;
    }
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return "Cliente: " + nome + " | " + 
                "CPF: " + cpf + " | " +
                "Data nascimento: " + dateFormat.format(dataNascimento) + " | " + 
                "Telefone: " + telefone + " | " +
                "Email: " + email;
    }
}
