package com.mycompany.agenciadeviagens.model;

public class Destino {
    
    private int idDestino;
    private String pais;
    private String cidade;
    
    public Destino(){}
    
    public Destino(int idDestino){
        this.idDestino = idDestino;
    }

    public Destino(int idDestino, String pais, String cidade) {
        this.idDestino = idDestino;
        this.pais = pais;
        this.cidade = cidade;
    }
    
    public int getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getDestinacao(){
        return cidade + ", " + pais;
    }
    
    @Override
    public String toString(){
        return "Destino: " + cidade + ", " + pais + " | " + 
                "ID: " + idDestino;
    }
}
