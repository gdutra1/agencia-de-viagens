package com.mycompany.agenciadeviagens.model;

public class Hotel {
    
    private int idHotel;
    private String nome;
    private String endereco;
    private int estrelas;
    private float valorDiaria;
    
    public Hotel(){}
    
    public Hotel(int idHotel){
        this.idHotel = idHotel;
    }

    public Hotel(int idHotel, String nome, String endereco, int estrelas, float valorDiaria) {
        this.idHotel = idHotel;
        this.nome = nome;
        this.endereco = endereco;
        this.estrelas = estrelas;
        this.valorDiaria = valorDiaria;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getEstrelas() {
        return estrelas;
    }

    public void setEstrelas(int estrelas) {
        this.estrelas = estrelas;
    }

    public float getValorDiaria() {
        return valorDiaria;
    }

    public void setValorDiaria(float valorDiaria) {
        this.valorDiaria = valorDiaria;
    }
    
    @Override
    public String toString(){
        return "Hotel: " + nome + " | " + 
                "ID: " + idHotel + " | " +
                "Endereço: " + endereco + " | " +
                "Estrelas: " + estrelas + " | " +
                "Valor da diária: " + valorDiaria;
    }
}
