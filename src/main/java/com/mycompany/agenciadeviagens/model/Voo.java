package com.mycompany.agenciadeviagens.model;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Voo {
    
    private int idVoo;
    private float preco;
    private Date diaSaida;
    private String localSaida;
    private Destino destinacao;
    
    public Voo(){}
    
    public Voo(int idVoo){
        this.idVoo = idVoo;
    }

    public Voo(int idVoo, float preco, Date diaSaida, String localSaida, Destino destinacao) {
        this.idVoo = idVoo;
        this.preco = preco;
        this.diaSaida = diaSaida;
        this.localSaida = localSaida;
        this.destinacao = destinacao;
    }

    public int getIdVoo() {
        return idVoo;
    }

    public void setIdVoo(int idVoo) {
        this.idVoo = idVoo;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public Date getDiaSaida() {
        return diaSaida;
    }

    public void setDiaSaida(Date diaSaida) {
        this.diaSaida = diaSaida;
    }

    public String getLocalSaida() {
        return localSaida;
    }

    public void setLocalSaida(String localSaida) {
        this.localSaida = localSaida;
    }

    public Destino getDestinacao() {
        return destinacao;
    }

    public void setDestinacao(Destino destinacao) {
        this.destinacao = destinacao;
    }
    
    @Override
    public String toString(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        
        return "Voo de " + localSaida + " para " + destinacao.getDestinacao() + " | " +
                "ID: " + idVoo + " | " +
                "Preço: " + preco + " | " +
                "Dia de saída: " + dateFormat.format(diaSaida);
    }
}
